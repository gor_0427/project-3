/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update();
int initFromFile(const string& fname);
void mainLoop();
void dumpState(FILE* f);

char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	mainLoop();
	return 0;
}

int initFromFile(
{
	FILE* f = fopen("/project-3/tests/1");

	g[i][j] = false;
	char c;
	while(fread(&c,1,1,f) != 0)
	{
		length = v1.size();
		if(c == "\n")
		{
			g.push_back(v1);
			v1.clear();
		}
		else
		{
			if(c == ".")
				v1.push_back(false);
			else
				v1.push_back(true);
		}
	fclose(f);
	}
}

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g,const vector<bool> v1)
{
	width = g.size();
	length = v1.size();
	int neighbor = 0;
	if(g[i][j] == true)
	{
		if(v2[i][(j-1+width)%width] == true)
      neighbor++;
    if(v2[i][(j+1)%width] == true)
      neighbor++;
    if(v2[(i-1+length)%length][j]) == true)
      neighbor++;
    if(v2[(i+1)%length][j] == true)
      neighbor++;
    if(v2[(i-1+length)%length][(j-1+width)%width] == true)
      neighbor++;
		if(v2[(i+1)%length][(j-1+width)%width] == true)
			neighbor++;
	  if(v2[(i-1+length)%length][(j+1)%width] == true)
      neighbor++;
    if(v2[(i+1)%length][(j+1)%width] == true)
      neighbor++;

		if(neighbor < 2 || neighbor > 3)
      return false;
    else
			return true;
	}
	else (g[i][j] == false)
	{
		if(v2[i][(j-1+width)%width] == true)
      neighbor++;
    if(v2[i][(j+1)%width] == true)
      neighbor++;
    if(v2[(i-1+length)%length][j]) == true)
      neighbor++;
    if(v2[(i+1)%length][j] == true)
      neighbor++;
    if(v2[(i-1+length)%length][(j-1+width)%width] == true)
      neighbor++;
    if(v2[(i+1)%length][(j-1+width)%width] == true)
      neighbor++;
    if(v2[(i-1+length)%length][(j+1)%width] == true)
      neighbor++;
    if(v2[(i+1)%length][(j+1)%width] == true)
      neighbor++;

	  if(neighbor == 3)
      return true;
    else
      return false;
	}
}

void dumpState(FILE*f,vector<vector<bool> > v2,vector<bool> v1)
{
	string wfilename =  "/tmp/gol-world-current";
	char c;
	bool state;

	for(j = 0;j < v2.size();j++)
	{
		for(i = 0; i < v1.size();i++)
		{
			state = nbrCount(i,j,v2,v1,v2[i][j])
			if(state = true)
			  c = "o";
		  else if(state = false)
			  c = ".";

		  fwrite(&c,1,1,f);
		}
	}

	fclose(f);
}
void mainLoop()
{
  while()
	{
		initFromFile(initfilename);
		dumpState(wfilename);
		update();
	}
	/* update, write, sleep */
}
